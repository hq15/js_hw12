/*
1. setTimeout() - позволяет один раз вызвать функцию через опеределенный промежуток времени, а setInterval() - позволяет регулярно вызывать функцию с повтором этого вызова через определенный промежуток времени.
2. Eсли в функцию setTimeout() передать нулевую задержку то это означает, что  выполнение «немедленно» или, точнее, как можно скорее. Планирование с нулевой задержкой используется для вызовов которые должны быть исполнены как можно скорее, после завершения исполнения текущего кода. Но гарантий по соблюдению времени задержки нет, это зависит от загрузки процессора, находиждение вкладки в фоновом режиме, работа ноутбука от аккумулятора. Кроме этого минимальную задержку может ограничивать браузер («после пяти вложенных таймеров интервал должен составлять не менее четырёх миллисекунд»). Поэтому функция с нулевой задержкой сработает по приницпу как можно скорее но не мгновенно.
3. Так как setInterval () выполняет задачу постоянно, то в случае когда созданный цикл запуска нам уже не нужен, нужно очистить интервал вызовом функции clearInterval(), иначе цикл может продлиться вплоть до перегрузки страницы.
*/

const slidesImg = document.querySelectorAll(".image-to-show");
let = currentImg = 0;
let slideInterval = setInterval(nextImg, 3000);

function nextImg() {
  proceedButton.setAttribute("disabled", "disabled");
  slidesImg[currentImg].className = "image-to-show";
  currentImg = (currentImg + 1) % slidesImg.length;
  slidesImg[currentImg].className = "showing";
}

pauseButton = document.querySelector(".pause");
proceedButton = document.querySelector(".proceed");

function pauseSlideshow() {
  clearInterval(slideInterval);
}

function playSlideshow() {
  slideInterval = setInterval(nextImg, 3000);
}

pauseButton.addEventListener("click", function (event) {
  pauseSlideshow();
  pauseButton.setAttribute("disabled", "disabled");
  proceedButton.removeAttribute("disabled");
});

proceedButton.addEventListener("click", function (event) {
  playSlideshow();
  proceedButton.setAttribute("disabled", "disabled");
  pauseButton.removeAttribute("disabled");
});
